// main.go
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type MyEvent struct {
	Name string `json:"name"`
}

func init() {
	//runs before handler
	fmt.Println("Init")

}

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	//lambda request handler, request body is event, from lambda proxy integration
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	//create sqs session
	svc := sqs.New(sess)

	//todo: implement queue url through env var
	queueURL := "https://sqs.us-east-1.amazonaws.com/785054438741/webhook-go"
	timeout := int64(0)

	//sqs read message from sqs lambda trigger
	//todo replace _ with msgResult as return response (avoiding undec var err)
	_, _ = svc.ReceiveMessage(&sqs.ReceiveMessageInput{
		AttributeNames: []*string{
			aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
		},
		MessageAttributeNames: []*string{
			aws.String(sqs.QueueAttributeNameAll),
		},
		QueueUrl:            &queueURL,
		MaxNumberOfMessages: aws.Int64(1),
		VisibilityTimeout:   &timeout,
	})

	//post webhoook
	data := url.Values{
		"name":       {"John Doe"},
		"occupation": {"gardener"},
	}

	resp, err := http.PostForm("https://david.requestcatcher.com", data)

	if err != nil {
		log.Fatal(err)
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	fmt.Println(res["form"])

	//set http response
	response := &events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       "success",
	}

	return response, err
}

func main() {
	lambda.Start(HandleRequest)
}
