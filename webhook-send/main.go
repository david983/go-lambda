// main.go
package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

var sess *session.Session
var svc *sqs.SQS
var res *events.APIGatewayProxyResponse

func init() {
	//initialize connection to sqs before handler is invoked
	fmt.Println("Init")

	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
	svc = sqs.New(sess)

}

func HandleRequest(ctx context.Context, event events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	//request handler, pushes json to sqs
	queueURL := "https://sqs.us-east-1.amazonaws.com/785054438741/webhook-go"

	sqs_response, err := svc.SendMessage(&sqs.SendMessageInput{

		MessageBody: aws.String(event.Body),
		QueueUrl:    &queueURL,
	})

	if err != nil {
		res = &events.APIGatewayProxyResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       err.Error(),
		}
	} else {
		res = &events.APIGatewayProxyResponse{
			StatusCode: http.StatusOK,
			Body:       *sqs_response.MD5OfMessageSystemAttributes,
		}
	}

	return res, err
}

func main() {
	lambda.Start(HandleRequest)
}
